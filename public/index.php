<?php
session_start();

$_SESSION["_csrf"] = bin2hex(random_bytes(32));

?>

<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

        <title>Demo App</title>
    </head>
    <body>
        <div class="container mt-4">
            <?php include __DIR__ . '/../partials/messages.php' ?>
            <div class="card">
                <div class="card-body">
                    <form action="results.php" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="_csrf" value="<?php echo $_SESSION['_csrf'] ?>">
                        <label for="csv_file" class="form-label">Choose CSV file</label>
                        <input class="form-control" type="file" id="csv_file" name="csv_file">
                        <div class="mt-4">
                            <button class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>