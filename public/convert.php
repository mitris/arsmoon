<?php

$info = fopen(__DIR__ . '/../data/metadata.csv', 'r');

$codes_by_country = [];
$codes_by_phone = [];
while (($data = fgetcsv($info, 1000, ';')) !== false) {
    $codes_by_country[trim($data[1])] = trim($data[0]);
    $codes_by_phone[trim($data[0])] = trim($data[1]);
}

file_put_contents(__DIR__ . '/../data/codes_by_country.json', json_encode($codes_by_country));
file_put_contents(__DIR__ . '/../data/codes_by_phone.json', json_encode($codes_by_phone));