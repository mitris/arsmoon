<?php
session_start();

if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    $_SESSION['message_danger'] = $_SERVER['REQUEST_METHOD'] . ' method doesn\'t support.';

    header('Location: /');
    exit;
}

if (!$_POST['_csrf'] || $_POST['_csrf'] != $_SESSION['_csrf']) {
    $_SESSION['message_danger'] = 'Token mismatch.';

    header('Location: /');
    exit;
}

if ($_FILES['csv_file'] && !$_FILES['csv_file']['tmp_name']) {
    $_SESSION['message_danger'] = 'You need to upload file.';

    header('Location: /');
    exit;
}

if ($_FILES['csv_file']['type'] != 'text/csv') {
    $_SESSION['message_danger'] = 'Incorrect file format. Only CSV file type is accepted.';

    header('Location: /');
    exit;
}

try {
    $handle = fopen($_FILES['csv_file']['tmp_name'], 'r');
    $result = [];
    $lookup_ips = [];

    while (($data = fgetcsv($handle, 1000, ',')) !== false) {
        if (count($data) != 5) {
            $_SESSION['message_danger'] = 'Incorrect CSV structure.';

            header('Location: /');
            exit;
        }

        $result[$data[0]]['total_calls_duration'] += $data[2];
        $result[$data[0]]['total_calls']++;
        $result[$data[0]]['calls'][$data[4]][] = [
            'phone' => $data[3],
            'duration' => $data[2],
        ];
        if (!in_array($data[4], $lookup_ips)) {
            $lookup_ips[] = $data[4];
        }
    }
    fclose($handle);
} catch (Exception $e) {
    $_SESSION['message_danger'] = 'Can\'t open uploaded file.';

    header('Location: /');
    exit;
}

try {
    $response = file_get_contents('http://ip-api.com/batch?fields=countryCode,query', false, stream_context_create([
        'http' => [
            'method' => 'POST',
            'header' => 'Content-Type: application/json',
            'content' => json_encode($lookup_ips)
        ]
    ]));
} catch (Exception $e) {
    echo $e->getMessage();
    exit;
}

$ip_details = json_decode($response, true);

$country_codes_by_ip = [];
foreach ($ip_details as $ip_detail) {
    $country_codes_by_ip[$ip_detail['query']] = $ip_detail['countryCode'];
}

try {
    $json = file_get_contents(__DIR__ . '/../data/codes_by_country.json');
} catch (Exception $e) {
    echo $e->getMessage();
    exit;
}

$phone_codes_by_country = json_decode($json, JSON_OBJECT_AS_ARRAY);

$summary_result = [];
foreach ($result as $customer => $item) {
    $summary_result[$customer]['total_calls_duration'] = $item['total_calls_duration'];
    $summary_result[$customer]['total_calls'] = $item['total_calls'];
    foreach ($item['calls'] as $call_ip => $phones) {
        if (isset($country_codes_by_ip[$call_ip])) {
            $country_code = $country_codes_by_ip[$call_ip];
            foreach ($phones as $phone_info) {
                $phone_code = $phone_codes_by_country[$country_code];
                if (substr($phone_info['phone'], 0, strlen($phone_code)) == $phone_code) {
                    $summary_result[$customer]['same_calls']++;
                    $summary_result[$customer]['same_calls_duration'] += $phone_info['duration'];
                }
            }
        }
    }
}
?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Results</title>
</head>
<body>
<div class="container">
    <div class="card mt-4">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Customer ID</th>
                <th>Calls with same continent</th>
                <th>Calls duration with same continent</th>
                <th>Total calls</th>
                <th>Total calls duration</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($summary_result as $customer => $info): ?>
                <tr>
                    <td><?php echo $customer ?></td>
                    <td><?php echo $info['same_calls'] ?: '-' ?></td>
                    <td><?php echo $info['same_calls_duration'] ?: '-' ?></td>
                    <td><?php echo $info['total_calls'] ?></td>
                    <td><?php echo $info['total_calls_duration'] ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <div class="p-3 pt-0">
            <a href="/" class="btn btn-primary">Back to upload</a>
        </div>
    </div>
</div>
</body>
</html>