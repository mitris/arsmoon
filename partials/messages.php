<?php

foreach (['danger', 'success', 'primary'] as $type) {
    if (isset($_SESSION['message_' . $type]) && $message = $_SESSION['message_' . $type]) {
        echo '<div class="alert alert-' . $type . '">' . $message . '</div>';

        unset($_SESSION['message_' . $type]);
    }
}